FROM debian:stable
VOLUME /source
WORKDIR /source
RUN apt-get update && \
# build essential allows to create executables with C compiler
    apt-get -y install vim file wget tar gcc make g++ libxml2-dev build-essential && \
    wget http://fr2.php.net/get/php-7.1.8.tar.gz/from/this/mirror && \
    tar xvf mirror && \
    cd php-7.1.8 && \
    CC="`which gcc`" CFLAGS=`-O0 -g -fsanitize=address` ./configure --disable-shared --enable-wddx && \
    make && \
    make install
COPY repro2.wddx .
COPY wddx.php .
EXPOSE 3000